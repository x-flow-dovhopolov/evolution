﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CurrencyUI : MonoBehaviour, IObserver
{
    #region fields
    
    public static CurrencyUI Instance { get; protected set; }
    
    [Header("UI")]
    public Text armyText;
    public Text foodText;
    public Text coinText;
    public Text gemsText;
    
    #endregion
    
    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else if (Instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        GameManager.Instance.Attach(this);
    }

    public void OpenSettings()
    {
        // TODO settings canvas menu
    }

    public void UpdateUICoin(int subject)
    {
        coinText.text = "" + subject;

    }
}
