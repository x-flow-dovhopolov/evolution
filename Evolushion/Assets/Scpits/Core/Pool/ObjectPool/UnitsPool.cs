using System.Collections.Generic;

namespace UnityEngine.UI
{
   public class UnitsPool : ObjectPool<UnitsPool, UnitObject, Vector2>
    {
        static protected Dictionary<GameObject, UnitsPool> PoolInstances = new Dictionary<GameObject, UnitsPool>();

        private void Awake()
        {
            //This allow to make Pool manually added in the scene still automatically findable & usable
            if(prefab != null && !PoolInstances.ContainsKey(prefab))
                PoolInstances.Add(prefab, this);
        }

        private void OnDestroy()
        {
            PoolInstances.Remove(prefab);
        }

        //initialPoolCount is only used when the objectpool don't exist
        static public UnitsPool GetObjectPool(GameObject prefab, int initialPoolCount = 10)
        {
            UnitsPool objPool = null;
            if (!PoolInstances.TryGetValue(prefab, out objPool))
            {
                GameObject obj = new GameObject(prefab.name + "_Pool");
                objPool = obj.AddComponent<UnitsPool>();
                objPool.prefab = prefab;
                objPool.initialPoolCount = initialPoolCount;

                PoolInstances[prefab] = objPool;
            }

            return objPool;
        }
    }

    public class UnitObject : PoolObject<UnitsPool, UnitObject, Vector2>
    {
        public Transform transform;
        public Rigidbody2D rigidbody2D;
        public SpriteRenderer spriteRenderer;
        public Unit bullet;

        protected override void SetReferences()
        {
            transform = instance.transform;
            rigidbody2D = instance.GetComponent<Rigidbody2D> ();
            spriteRenderer = instance.GetComponent<SpriteRenderer> ();
            bullet = instance.GetComponent<Unit>();
            //bullet.bulletPoolObject = this;
            //bullet.mainCamera = Object.FindObjectOfType<Camera> ();
        }

        public override void WakeUp(Vector2 position)
        {
            transform.position = position;
            instance.SetActive(true);
        }

        public override void Sleep()
        {
            instance.SetActive(false);
        }
    }
}