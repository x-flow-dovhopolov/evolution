using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.PlayerLoop;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour, ISubject
{
    #region fields

    public static GameManager Instance;
    
    public GameData gameData;
    public bool IsGameLoading { get; private set; }
    public bool SettingsLoaded{ get; set; }
    public bool PlayerDataLoaded{ get; set; }
    
    private DataLoader _dataLoader;
    public DataHandler dataHandler;
    
    public delegate DataHandler OnDataLoaded();
    public event OnDataLoaded onDataLoaded; // Ne dergetsya nigde
    
    #endregion
    
    #region Methods
    
    void Awake()
    {
        if (Instance == null)
            Instance = this;
        else if (Instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        Screen.orientation = ScreenOrientation.Portrait;
        _dataLoader = new DataLoader();
        gameData = new GameData();
        
        LoadData();
    }
    
    private void LoadMainGameScene()
    {
        SceneManager.LoadScene("Adventure");
    }
    
    private void LoadData()
    {
        IsGameLoading = true;
        
        StartCoroutine(SplashSceneLoader.Instance.LoadSavedScene());
        StartCoroutine(GotLoadingResponse());
        _dataLoader.StartLoad();
    }
    
    private IEnumerator GotLoadingResponse()
    {
        //Emulator downloading 2 second
        yield return new WaitForSeconds(0.1f);
        
        while (!SettingsLoaded && !PlayerDataLoaded)
        {
            yield return new WaitForSeconds(1f);
        }
        
        SplashSceneLoader.Instance.isLoading = false;
        
        LoadMainGameScene();
        yield return null;
    }
    
    private void OnApplicationQuit()
    {
        //dataHandler.json = gameData.Save();
        _dataLoader.Save();
    }
    
    public void SetCoinCount(int coinCount)
    {
        dataHandler.GoldCount += coinCount;
        SpecialBusinessLogic();
    }
    
    
    #endregion
    
    #region Observer data

    private List<IObserver> _observers = new List<IObserver>();

    public void Attach(IObserver observer)
    {
        this._observers.Add(observer);
        //не по канону
        observer.UpdateUICoin(dataHandler.GoldCount);
    }

    public void Detach(IObserver observer)
    {
        this._observers.Remove(observer);
    }

    public void Notify()
    {

        foreach (var observer in _observers)
        {
            observer.UpdateUICoin(dataHandler.GoldCount);
        }
    }

    public void SpecialBusinessLogic()
    {
        this.Notify();
    }
    #endregion
}