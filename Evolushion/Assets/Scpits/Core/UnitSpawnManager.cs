﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnitSpawnManager : MonoBehaviour
{
    #region Fields
    [HideInInspector]
    public static UnitSpawnManager instance = null;
    [SerializeField]
    private int _unitsOnScene = 0;
    [SerializeField]
    private int _maxUnitsOnScene = 5;
    [SerializeField]
    private int _cooldownTime = 5;
    
    private bool IsLooping = true;
    
    public int CountUnitsOnScene
    {
        get => _unitsOnScene;
        set
        {
            if (value >= 0)
            {
                _unitsOnScene = value;
            }
        }
    }

    [SerializeField]
    public GameObject _prefab;
    #endregion

    #region Methods
    
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
    }

    public void StartSpawn()
    {
        StartCoroutine(StartSpawnUnits());
    }
    
    private IEnumerator StartSpawnUnits()
    {
        int i = 0;

        while (IsLooping)
        {
            i++;

            if (i == _cooldownTime)
            {
                i = 0;
                if (_unitsOnScene < _maxUnitsOnScene)
                {
                    Instantiate(_prefab, new Vector3(Random.Range(-2.5f, 2.5f), Random.Range(2f, -3.5f), 0),
                                                      Quaternion.identity);
                    //Vector3 vector3 = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2, Screen.height / 2,0));
                    /*GameObject singleUnit = ObjectPooler.Instance.GetPooledObject();
                    if (singleUnit != null) {
                        singleUnit.transform.position = new Vector3(Random.Range(-2.5f, 2.5f), Random.Range(2f, -3.5f), 0);
                        singleUnit.SetActive(true);
                    }*/
                    _unitsOnScene++;
                }
            }
            Debug.Log("yield return new WaitForSeconds(1f);");
            yield return new WaitForSeconds(1f);
        }
    }
    
    #endregion
    
    #region Collitions and clicker
    
    private float _fireRate = 0.5f;
    private float _canFire = -1f;
    
    private void Clicker(RaycastHit2D hit)
    {
        if(hit.transform == null)
            return;

        if (Time.time > _canFire)
        {
            _canFire = Time.time + _fireRate;
            hit.transform.GetComponent<Unit>().SpawnCoinByTouch();
        }
    }

    private void CheckCollisions(Vector3 v3)
    {
        RaycastHit2D[] hits = Physics2D.RaycastAll(v3, Vector2.zero);

        if (hits.Length == 2)
        {
            if (hits[0].transform.gameObject.name == hits[1].transform.gameObject.name)
            {
                GameObject objectToCreate = hits[0].transform.GetComponent<Unit>().NextUnitLevel;
                Debug.Log(objectToCreate);
                Destroy(hits[0].transform.gameObject);
                Destroy(hits[1].transform.gameObject);

                if (objectToCreate != null)
                {
                    Instantiate(objectToCreate, v3, Quaternion.identity);
                    UnitSpawnManager.instance.CountUnitsOnScene -= 1;
                }
            }
        }
    }

    #endregion
}