﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BuildingManager : MonoBehaviour
{
    #region Fields
    public static BuildingManager instance;

    public int millPrice = 100;

    [SerializeField]
    private List<GameObject> _buildList = new List<GameObject>();
    #endregion
    
    #region Methods
    
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        SetBuildingsOnMap();
    }
    
    private void Update()
    {    
        // костыль
        if (!SceneManager.GetActiveScene().name.Equals("Adventure"))
        {
            _buildList[0].active = false;
            return;
        }
        
        if (Input.GetMouseButton(0))
        { 
            var v3 = Input.mousePosition;
            v3.z = 10f;
            v3 = Camera.main.ScreenToWorldPoint(v3);

            RaycastHit2D hit = Physics2D.Raycast(v3, Vector2.zero);
            if (hit.collider != null)
            {
                hit.transform.GetComponent<Building>().GoToFarmScene();
            }
        }
    }
    
    public void SetBuildigIsBuild()
    {
        _buildList[0].GetComponent<Building>().ChangeSprite();
    }

    public void SetBuildingsOnMap()
    {
        foreach (KeyValuePair<string, Building> keyValue in GameManager.Instance.gameData.GetBuildings())
        {
            if (keyValue.Value.GetID().Equals("ferma") && keyValue.Value.IsBuilt)
            {
                _buildList[0].GetComponent<Building>().ChangeSprite();
                keyValue.Value.IsBuilt = true;
            }
        }
    }
    #endregion
}